from conan import ConanFile

class BeamPositionTrackingRecipe(ConanFile):
    name = "beampositiontracking"
    executable = "ds_BeamPositionTracking"
    version = "1.0.13"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Falilou Thiam"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/motion/beampositiontracking/beampositiontracking.git"
    description = "BeamPositionTracking project"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "BeamPositionTracking/CMakeLists.txt", "BeamPositionTracking/src/*", "PluginInterfaces/*", "SensorPlugins/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        self.requires("opencv2core/[>=1.0]@soleil/stable")
        self.requires("opencv2imgproc/[>=1.0]@soleil/stable")
        self.requires("opencv2highgui/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
    